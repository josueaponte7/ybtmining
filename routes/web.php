<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/equipos', 'EquiposController@index')->name('equipos');
Route::post('/equipos', 'EquiposController@guardar')->name('equipos.save');

Route::get('/costo', 'CostoEquipoController@index')->name('costo');
Route::post('/costo', 'CostoEquipoController@guardar')->name('costo.save');

