@extends('layouts.template')

@section('dashboard')
    @if (session()->has('message'))
        <div id="success-alert" class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <form method="POST" autocomplete="off" role="form" id="frmbancos" action="{{ route('costo.save') }}">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Modelo</label>
                    <select name="modelo_id" id="modelo_id" class="form-control">
                        <option value="0">Seleccione</option>
                        @foreach ($modelos as $modelo)
                            <option value="{{ $modelo->id }}">{{ $modelo->modelo }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Costo($)</label>
                    <input type="text" name="costo" id="costo" class="form-control" placeholder="Costo($)">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Proveedor</label>
                    <select name="proveedor_id" id="proveedor_id" class="form-control">
                        <option value="0">Seleccione</option>
                        @foreach ($proveedores as $proveedor)
                            <option value="{{ $proveedor->id }}">{{ $proveedor->proveedor }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">BTC</label>
                    <input type="text" name="btc" id="btc" class="form-control" placeholder="BTC">
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
            <button type="button" class="btn btn-default">Cancel</button>
        </div>
    </form>
    @section('script')
        <script>
            $(document).ready(function(){
	            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
		            $("#success-alert").slideUp(500);
	            });

	            $( "#costo" ).blur(function() {
	            	var valor = parseFloat($(this).val());
		            $.getJSON("https://blockchain.info/es/ticker", function( data ) {
			            var precio = data.USD.last
                        var cant = parseFloat(valor/precio).toFixed(2);;
			            $('#btc').val(cant)
		            });
	            });
            });
        </script>
    @endsection
@endsection