@extends('layouts.template')

@section('dashboard')
    @if (session()->has('message'))
        <div id="success-alert" class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <form method="POST" autocomplete="off" role="form" id="frmbancos" action="{{ route('equipos.save') }}">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Modelo</label>
                    <select name="modelo_id" id="modelo_id" class="form-control">
                        <option value="0">Seleccione</option>
                        @foreach ($modelos as $modelo)
                            <option value="{{ $modelo->id }}">{{ $modelo->modelo }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Marca</label>
                    <input type="text" name="marca" id="marca" class="form-control" placeholder="Marca">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Precio costo</label>
                    <input type="text" name="precio_costo" id="precio_costo" class="form-control" placeholder="Precio costo">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Precio venta</label>
                    <input type="text" name="precio_venta" id="precio_venta" class="form-control" placeholder="Precio venta">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Potencia minado</label>
                    <input type="text" name="potencia_minado" id="potencia_minado" class="form-control" placeholder="Potencia minado">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Cantidad equipos</label>
                    <input type="text" name="cantidad" id="cantidad" class="form-control" placeholder="cantidad">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Tipo moneda</label>
                    <select name="tipo_moneda_id" id="tipo_moneda_id" class="form-control">
                        <option value="0">Seleccione</option>
                        @foreach ($tipo_monedas as $tipo_moneda)
                            <option value="{{ $tipo_moneda->id }}">{{ $tipo_moneda->tipo_moneda }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
            <button type="button" class="btn btn-default">Cancel</button>
        </div>
    </form>
    @section('scrip')
        <script>
            $(document).ready(function(){
            	alert('ff')
	            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
		            $("#success-alert").slideUp(500);
	            });
            })
        </script>
    @endsection
@endsection