<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li class="user-pro">
                <a href="doctor-schedule.html#" class="waves-effect">
                    <img src="{{asset('images/users/d1.jpg')}}" alt="user-img" class="img-circle">
                    <span class="hide-menu">{{ Auth::user()->name }}<span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="javascript:void(0)"><i class="ti-user"></i> Mi Perfil</a></li>
                    {{-- <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
                     <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>--}}
                    <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> Salir</a></li>
                </ul>
            </li>
            <li>
                <a href="{{route('home')}}" class="waves-effect">
                    <i class="fa fa-home"></i>
                    <span class="hide-menu">Inicio</span>
                </a>
            </li>
            @foreach (menu() as  $menu)
                <li>
                <a href="{{route($menu->route)}}" class="waves-effect">
                    <i class="{{$menu->icon}}"></i>
                    <span class="hide-menu">{{$menu->name}}</span>
                </a>
            </li>
            @endforeach
            {{--<li>
                <a href="{{route('equipos')}}" class="waves-effect">
                    <i class="ti-desktop"></i>
                    <span class="hide-menu">Registro de equipos</span>
                </a>
            </li>
            <li>
                <a href="{{route('costo')}}" class="waves-effect">
                    <i class="fa fa-usd"></i>
                    <span class="hide-menu">Costo de equipos {{prueba()}}</span>
                </a>
            </li>
            <li>
                <a href="{{route('equipos')}}" class="waves-effect">
                    <i class="fa fa-edit"></i>
                    <span class="hide-menu">Contratos</span>
                </a>
            </li>
            <li>
                <a href="{{route('equipos')}}" class="waves-effect">
                    <i class="ti-desktop"></i>
                    <span class="hide-menu">Bonificación</span>
                </a>
            </li>
            <li>
                <a href="{{route('equipos')}}" class="waves-effect">
                    <i class="fa fa-users"></i>
                    <span class="hide-menu">Usuarios</span>
                </a>
            </li>--}}
            {{--<li class="hide-menu">
                <a href="javacript:void(0);">
                    <span>Progress Report</span>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%" role="progressbar"> <span class="sr-only">85% Complete (success)</span> </div>
                    </div>
                    <span>Leads Report</span>
                    <div class="progress">
                        <div class="progress-bar progress-bar-danger" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%" role="progressbar"> <span class="sr-only">85% Complete (success)</span> </div>
                    </div>
                </a>
            </li>--}}
        </ul>
    </div>
</div>