<!DOCTYPE html>

<html lang="es">
    @include('layouts.head')
   <body>
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            @include('layouts.nav-header')
            @include('layouts.left-navbar-header')

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">{{ isset($titulo) ? $titulo: '' }}</h4>
                        </div>
                       {{-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
                            <ol class="breadcrumb">
                                <li><a href="doctor-schedule.html#">Hospital</a></li>
                                <li class="active">Doctor Schedule</li>
                            </ol>
                        </div>--}}
                        <!-- /.col-lg-12 -->
                    </div>
                    @yield('dashboard')
                </div>
                @include('layouts.right-sidebar')
            </div>
            <footer class="footer text-center"> 2017 &copy;ybtminning </footer>
        </div>
   </body>
   @include('layouts.script')

   @yield('script')
</html>