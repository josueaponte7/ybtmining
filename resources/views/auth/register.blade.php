@extends('layouts.ly-login')
@section('content')
  <section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box">
      <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('register') }}">
          {{ csrf_field() }}
        <a href="javascript:void(0)" class="text-center db">
          <img src="{{asset('images/eliteadmin-logo-dark.png')}}" alt="Home"/><br/>
          <img src="{{asset('images/eliteadmin-text-dark.png')}}" alt="Home"/>
        </a>
        <h3 class="box-title m-t-40 m-b-0">Regístrate ahora</h3>
        <small>Crea tu cuenta y disfruta</small>
        <div class="form-group m-t-20">
          <div class="col-xs-12">
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombre">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input id="password" type="password" class="form-control" name="password" required  placeholder="Password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
             <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required  placeholder="Confirmar Password">
          </div>
        </div>
        <div class="form-group" >
          <div class="col-md-12">
            <div class="checkbox checkbox-primary p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> Acuerdo con todos los <a href="#">Términos</a></label>
            </div>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Regístrate</button>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>¿Ya tienes una cuenta? <a href="/" class="text-primary m-l-5"><b>Inicia Sesión</b></a></p>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
@endsection
