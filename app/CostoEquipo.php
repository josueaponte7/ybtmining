<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostoEquipo extends Model
{
    protected $table = 'costo_equipos';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['modelo_id', 'costo', 'proveedor_id', 'btc'];
}
