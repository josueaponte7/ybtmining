<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipos extends Model
{
    protected $table = 'equipos';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['modelo_id', 'marca', 'potencia_minado', 'precio_costo', 'precio_venta', 'cantidad', 'tipo_moneda_id'];
}
