<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoMoneda extends Model
{
    protected $table = 'tipo_monedas';
    protected $hidden = ['created_at', 'updated_at'];
}
