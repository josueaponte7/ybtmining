<?php

namespace App\Http\Controllers;

use App\Equipos;
use App\Modelo;
use App\TipoMoneda;
use Illuminate\Http\Request;

class EquiposController extends Controller
{
    public function index()
    {
        $titulo = 'Registro de Equipos';
        $tipo_monedas = TipoMoneda::all();
        $modelos = Modelo::all();
        return view('equipos.registro', compact('titulo', 'tipo_monedas', 'modelos'));
    }

    public function guardar(Request $request)
    {
        $data = $request->all();
        $obj = new Equipos($data);
        $obj->save();
        return redirect()->back()->with('message', 'Registro con exito');
    }
}
