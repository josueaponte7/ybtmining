<?php

namespace App\Http\Controllers;

use App\CostoEquipo;
use App\Modelo;
use App\Proveedor;
use Illuminate\Http\Request;

class CostoEquipoController extends Controller
{
    public function index()
    {
        $titulo = 'Costos de Equipos';
        $modelos = Modelo::all();
        $proveedores = Proveedor::all();
        return view('equipos.costo', compact('titulo', 'tipo_monedas', 'modelos', 'proveedores'));
    }

    public function guardar(Request $request)
    {
        $data = $request->all();
        $obj = new CostoEquipo($data);
        $obj->save();
        return redirect()->back()->with('message', 'Registro con exito');
    }
}
