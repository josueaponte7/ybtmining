--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.15
-- Dumped by pg_dump version 9.4.15
-- Started on 2017-12-08 16:52:38 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1 (class 3079 OID 11861)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2079 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 1151908)
-- Name: costo_equipos; Type: TABLE; Schema: public; Owner: pagomovil; Tablespace: 
--

CREATE TABLE costo_equipos (
    id integer NOT NULL,
    costo double precision DEFAULT 0::double precision NOT NULL,
    proveedor character varying(150) NOT NULL,
    btc double precision DEFAULT 0::double precision NOT NULL,
    modelo_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE costo_equipos OWNER TO pagomovil;

--
-- TOC entry 184 (class 1259 OID 1151906)
-- Name: costo_equipos_id_seq; Type: SEQUENCE; Schema: public; Owner: pagomovil
--

CREATE SEQUENCE costo_equipos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE costo_equipos_id_seq OWNER TO pagomovil;

--
-- TOC entry 2080 (class 0 OID 0)
-- Dependencies: 184
-- Name: costo_equipos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pagomovil
--

ALTER SEQUENCE costo_equipos_id_seq OWNED BY costo_equipos.id;


--
-- TOC entry 181 (class 1259 OID 1151852)
-- Name: equipos; Type: TABLE; Schema: public; Owner: pagomovil; Tablespace: 
--

CREATE TABLE equipos (
    id integer NOT NULL,
    marca character varying(150) NOT NULL,
    potencia_minado character varying(150) NOT NULL,
    precio_costo double precision DEFAULT 0::double precision NOT NULL,
    precio_venta double precision DEFAULT 0::double precision NOT NULL,
    cantidad integer NOT NULL,
    tipo_moneda_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    modelo_id integer NOT NULL
);


ALTER TABLE equipos OWNER TO pagomovil;

--
-- TOC entry 180 (class 1259 OID 1151850)
-- Name: equipos_id_seq; Type: SEQUENCE; Schema: public; Owner: pagomovil
--

CREATE SEQUENCE equipos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE equipos_id_seq OWNER TO pagomovil;

--
-- TOC entry 2081 (class 0 OID 0)
-- Dependencies: 180
-- Name: equipos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pagomovil
--

ALTER SEQUENCE equipos_id_seq OWNED BY equipos.id;


--
-- TOC entry 174 (class 1259 OID 1151014)
-- Name: migrations; Type: TABLE; Schema: public; Owner: pagomovil; Tablespace: 
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO pagomovil;

--
-- TOC entry 173 (class 1259 OID 1151012)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: pagomovil
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migrations_id_seq OWNER TO pagomovil;

--
-- TOC entry 2082 (class 0 OID 0)
-- Dependencies: 173
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pagomovil
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- TOC entry 183 (class 1259 OID 1151900)
-- Name: modelos; Type: TABLE; Schema: public; Owner: pagomovil; Tablespace: 
--

CREATE TABLE modelos (
    id integer NOT NULL,
    modelo character varying(150) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE modelos OWNER TO pagomovil;

--
-- TOC entry 182 (class 1259 OID 1151898)
-- Name: modelos_id_seq; Type: SEQUENCE; Schema: public; Owner: pagomovil
--

CREATE SEQUENCE modelos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE modelos_id_seq OWNER TO pagomovil;

--
-- TOC entry 2083 (class 0 OID 0)
-- Dependencies: 182
-- Name: modelos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pagomovil
--

ALTER SEQUENCE modelos_id_seq OWNED BY modelos.id;


--
-- TOC entry 177 (class 1259 OID 1151033)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: pagomovil; Tablespace: 
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE password_resets OWNER TO pagomovil;

--
-- TOC entry 179 (class 1259 OID 1151844)
-- Name: tipo_monedas; Type: TABLE; Schema: public; Owner: pagomovil; Tablespace: 
--

CREATE TABLE tipo_monedas (
    id integer NOT NULL,
    tipo_moneda character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE tipo_monedas OWNER TO pagomovil;

--
-- TOC entry 178 (class 1259 OID 1151842)
-- Name: tipo_monedas_id_seq; Type: SEQUENCE; Schema: public; Owner: pagomovil
--

CREATE SEQUENCE tipo_monedas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipo_monedas_id_seq OWNER TO pagomovil;

--
-- TOC entry 2084 (class 0 OID 0)
-- Dependencies: 178
-- Name: tipo_monedas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pagomovil
--

ALTER SEQUENCE tipo_monedas_id_seq OWNED BY tipo_monedas.id;


--
-- TOC entry 176 (class 1259 OID 1151022)
-- Name: users; Type: TABLE; Schema: public; Owner: pagomovil; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    user_type integer DEFAULT 2
);


ALTER TABLE users OWNER TO pagomovil;

--
-- TOC entry 175 (class 1259 OID 1151020)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: pagomovil
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO pagomovil;

--
-- TOC entry 2085 (class 0 OID 0)
-- Dependencies: 175
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pagomovil
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 1929 (class 2604 OID 1151911)
-- Name: id; Type: DEFAULT; Schema: public; Owner: pagomovil
--

ALTER TABLE ONLY costo_equipos ALTER COLUMN id SET DEFAULT nextval('costo_equipos_id_seq'::regclass);


--
-- TOC entry 1925 (class 2604 OID 1151855)
-- Name: id; Type: DEFAULT; Schema: public; Owner: pagomovil
--

ALTER TABLE ONLY equipos ALTER COLUMN id SET DEFAULT nextval('equipos_id_seq'::regclass);


--
-- TOC entry 1921 (class 2604 OID 1151017)
-- Name: id; Type: DEFAULT; Schema: public; Owner: pagomovil
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- TOC entry 1928 (class 2604 OID 1151903)
-- Name: id; Type: DEFAULT; Schema: public; Owner: pagomovil
--

ALTER TABLE ONLY modelos ALTER COLUMN id SET DEFAULT nextval('modelos_id_seq'::regclass);


--
-- TOC entry 1924 (class 2604 OID 1151847)
-- Name: id; Type: DEFAULT; Schema: public; Owner: pagomovil
--

ALTER TABLE ONLY tipo_monedas ALTER COLUMN id SET DEFAULT nextval('tipo_monedas_id_seq'::regclass);


--
-- TOC entry 1922 (class 2604 OID 1151025)
-- Name: id; Type: DEFAULT; Schema: public; Owner: pagomovil
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2071 (class 0 OID 1151908)
-- Dependencies: 185
-- Data for Name: costo_equipos; Type: TABLE DATA; Schema: public; Owner: pagomovil
--



--
-- TOC entry 2086 (class 0 OID 0)
-- Dependencies: 184
-- Name: costo_equipos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pagomovil
--

SELECT pg_catalog.setval('costo_equipos_id_seq', 1, true);


--
-- TOC entry 2067 (class 0 OID 1151852)
-- Dependencies: 181
-- Data for Name: equipos; Type: TABLE DATA; Schema: public; Owner: pagomovil
--

INSERT INTO equipos (id, marca, potencia_minado, precio_costo, precio_venta, cantidad, tipo_moneda_id, created_at, updated_at, modelo_id) VALUES (6, 'sfsfs', '353', 3223, 345, 353, 2, '2017-12-06 12:32:29', '2017-12-06 12:32:29', 1);


--
-- TOC entry 2087 (class 0 OID 0)
-- Dependencies: 180
-- Name: equipos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pagomovil
--

SELECT pg_catalog.setval('equipos_id_seq', 6, true);


--
-- TOC entry 2060 (class 0 OID 1151014)
-- Dependencies: 174
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: pagomovil
--

INSERT INTO migrations (id, migration, batch) VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO migrations (id, migration, batch) VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO migrations (id, migration, batch) VALUES (3, '2017_12_04_110158_create_tipo_monedas_table', 2);
INSERT INTO migrations (id, migration, batch) VALUES (4, '2017_12_04_110354_create_equipos_table', 2);
INSERT INTO migrations (id, migration, batch) VALUES (7, '2017_12_06_115508_create_modelos_table', 3);
INSERT INTO migrations (id, migration, batch) VALUES (8, '2017_12_06_115620_create_costo_equipos_table', 3);
INSERT INTO migrations (id, migration, batch) VALUES (9, '2017_12_06_121920_add_modelos_to_equipos_table', 4);


--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 173
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pagomovil
--

SELECT pg_catalog.setval('migrations_id_seq', 9, true);


--
-- TOC entry 2069 (class 0 OID 1151900)
-- Dependencies: 183
-- Data for Name: modelos; Type: TABLE DATA; Schema: public; Owner: pagomovil
--

INSERT INTO modelos (id, modelo, created_at, updated_at) VALUES (1, 'Antiminer S9', NULL, NULL);


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 182
-- Name: modelos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pagomovil
--

SELECT pg_catalog.setval('modelos_id_seq', 1, true);


--
-- TOC entry 2063 (class 0 OID 1151033)
-- Dependencies: 177
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: pagomovil
--



--
-- TOC entry 2065 (class 0 OID 1151844)
-- Dependencies: 179
-- Data for Name: tipo_monedas; Type: TABLE DATA; Schema: public; Owner: pagomovil
--

INSERT INTO tipo_monedas (id, tipo_moneda, created_at, updated_at) VALUES (1, 'BITCOIN', NULL, NULL);
INSERT INTO tipo_monedas (id, tipo_moneda, created_at, updated_at) VALUES (2, 'DASH', NULL, NULL);
INSERT INTO tipo_monedas (id, tipo_moneda, created_at, updated_at) VALUES (3, 'LITECOIN', NULL, NULL);


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 178
-- Name: tipo_monedas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pagomovil
--

SELECT pg_catalog.setval('tipo_monedas_id_seq', 3, true);


--
-- TOC entry 2062 (class 0 OID 1151022)
-- Dependencies: 176
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: pagomovil
--

INSERT INTO users (id, name, email, password, remember_token, created_at, updated_at, user_type) VALUES (1, 'Josue', 'josueaponte7@gmail.com', '$2y$10$xWDbswAYiPrAM/79wE8Vg.IKWFuzLAERdn3Dmm440zQjv1TSRzbYO', 'sHj74CKtTH6gxtVnasawoyxvYJUxD5tIlfKPNAFnJJmps4lqJiUhpgiysnmT', '2017-11-05 17:57:55', '2017-11-05 17:57:55', 2);
INSERT INTO users (id, name, email, password, remember_token, created_at, updated_at, user_type) VALUES (4, 'Administrador', 'administrador@gmail.com', '$2y$10$/4VBk4MU0ZgUmYBOhR6b/ewkuArVbAnhmvCvt.rwn8qdOeuS2eZ.q', 'iHQNJBImRai1xLz9hECBXWe8oQPDwOGpTat3mlKe84JLb3JymJwoSX2HHQTF', '2017-12-04 01:32:05', '2017-12-04 01:32:05', 1);


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 175
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pagomovil
--

SELECT pg_catalog.setval('users_id_seq', 5, true);


--
-- TOC entry 1946 (class 2606 OID 1151915)
-- Name: costo_equipos_pkey; Type: CONSTRAINT; Schema: public; Owner: pagomovil; Tablespace: 
--

ALTER TABLE ONLY costo_equipos
    ADD CONSTRAINT costo_equipos_pkey PRIMARY KEY (id);


--
-- TOC entry 1942 (class 2606 OID 1151859)
-- Name: equipos_pkey; Type: CONSTRAINT; Schema: public; Owner: pagomovil; Tablespace: 
--

ALTER TABLE ONLY equipos
    ADD CONSTRAINT equipos_pkey PRIMARY KEY (id);


--
-- TOC entry 1933 (class 2606 OID 1151019)
-- Name: migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: pagomovil; Tablespace: 
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 1944 (class 2606 OID 1151905)
-- Name: modelos_pkey; Type: CONSTRAINT; Schema: public; Owner: pagomovil; Tablespace: 
--

ALTER TABLE ONLY modelos
    ADD CONSTRAINT modelos_pkey PRIMARY KEY (id);


--
-- TOC entry 1940 (class 2606 OID 1151849)
-- Name: tipo_monedas_pkey; Type: CONSTRAINT; Schema: public; Owner: pagomovil; Tablespace: 
--

ALTER TABLE ONLY tipo_monedas
    ADD CONSTRAINT tipo_monedas_pkey PRIMARY KEY (id);


--
-- TOC entry 1935 (class 2606 OID 1151032)
-- Name: users_email_unique; Type: CONSTRAINT; Schema: public; Owner: pagomovil; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 1937 (class 2606 OID 1151030)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: pagomovil; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 1938 (class 1259 OID 1151039)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: pagomovil; Tablespace: 
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- TOC entry 1949 (class 2606 OID 1151916)
-- Name: costo_equipos_modelo_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pagomovil
--

ALTER TABLE ONLY costo_equipos
    ADD CONSTRAINT costo_equipos_modelo_id_foreign FOREIGN KEY (modelo_id) REFERENCES modelos(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1948 (class 2606 OID 1151921)
-- Name: equipos_modelo_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pagomovil
--

ALTER TABLE ONLY equipos
    ADD CONSTRAINT equipos_modelo_id_foreign FOREIGN KEY (modelo_id) REFERENCES modelos(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1947 (class 2606 OID 1151860)
-- Name: equipos_tipo_moneda_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: pagomovil
--

ALTER TABLE ONLY equipos
    ADD CONSTRAINT equipos_tipo_moneda_id_foreign FOREIGN KEY (tipo_moneda_id) REFERENCES tipo_monedas(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2078 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-12-08 16:52:38 -04

--
-- PostgreSQL database dump complete
--

