<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostoEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costo_equipos', function (Blueprint $table) {
            $table->increments('id');
            $table->float('costo', 10, 2)->default('0.00');
            $table->string('proveedor', 150);
            $table->float('btc', 10, 2)->default('0.00');
            $table->integer('modelo_id')->unsigned();
            $table->timestamps();
            $table->foreign('modelo_id')->references('id')->on('modelos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costo_equipos');
    }
}
