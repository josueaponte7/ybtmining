<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBitacoraCostosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora_costos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('proveedor', 150);
            $table->integer('cantidad')->unsigned();
            $table->float('precio_costo', 10, 2)->default('0.00');
            $table->float('precio_venta', 10, 2)->default('0.00');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitacora_costos');
    }
}
