<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('modelo', 150);
            $table->string('marca', 150);
            $table->string('potencia_minado', 150);
            $table->float('precio_costo', 10, 2)->default('0.00');
            $table->float('precio_venta', 10, 2)->default('0.00');
            $table->integer('cantidad')->unsigned();
            $table->integer('tipo_moneda_id')->unsigned();
            $table->timestamps();
            $table->foreign('tipo_moneda_id')->references('id')->on('tipo_monedas')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipos');
    }
}
